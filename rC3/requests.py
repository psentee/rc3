import os.path

import cachecontrol  # type: ignore
import cachecontrol.caches.file_cache  # type: ignore
import requests

CACHE_DIR = os.path.expanduser("~/.cache/rC3")

session = cachecontrol.CacheControl(
    requests.session(),
    cache=cachecontrol.caches.file_cache.FileCache(CACHE_DIR),
)
