import sys

from .menu import StaticItem, StaticMenu
from .recording import RecordingMenu
from .relive import ReliveMenu
from .stream import StreamMenu
from .upgrade import upgrade

MENU = StaticMenu(
    "",
    StreamMenu(),
    ReliveMenu(),
    RecordingMenu(),
    StaticItem("Upgrade", upgrade),
    StaticItem("Quit", lambda: sys.exit(0)),
)
