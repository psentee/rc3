import typing as t

from rich.console import RenderableType

from .fzf import MenuLine, fzf


class Item(MenuLine, t.Protocol):
    def play(self) -> None:
        ...


class Menu:
    menu_line: RenderableType
    menu_description: t.Optional[RenderableType] = None
    prompt: t.Optional[str] = None
    header: t.Optional[str] = None

    def items(self) -> t.Sequence[Item]:
        ...

    def play(self) -> None:
        while True:
            items = self.items()  # always refresh
            result = fzf(items, prompt=self.prompt, header=self.header)
            if result is None:
                break
            result.play()


class StaticItem:
    menu_line: str
    menu_description: t.Optional[str]
    _impl: tuple[t.Callable[[], None]]

    def __init__(
        self, name: str, impl: t.Callable[[], None], description: t.Optional[str] = None
    ) -> None:
        self._impl = (impl,)
        self.menu_line = name
        self.menu_description = description

    def play(self) -> None:
        impl = self._impl[0]
        impl()


class StaticMenu(Menu):
    _items: tuple[Item, ...]

    def __init__(
        self,
        title: str,
        *items: Item,
        prompt: t.Optional[str] = None,
        header: t.Optional[str] = None
    ) -> None:
        self.menu_line = title
        self.prompt = prompt
        self.header = header
        self._items = items

    def items(self) -> tuple[Item, ...]:
        return self._items
