import re
import typing as t
from datetime import timedelta

import dateutil.parser
import dateutil.tz
from markdownify import markdownify
from rich.markdown import Markdown

FAILDUMP_FILE = "fail.html"
TZ = dateutil.tz.gettz("Europe/Berlin")


def parse_duration(hhmmss: str) -> timedelta:
    ddt = dateutil.parser.parse(hhmmss)
    return timedelta(hours=ddt.hour, minutes=ddt.minute, seconds=ddt.second)


def format_duration(td: timedelta, emoji: str = ":stopwatch:") -> str:
    minutes = int(td.total_seconds() / 60)
    return f"{emoji} {minutes}m"


_T = t.TypeVar("_T")


def first(
    sequence: t.Iterable[_T], condition: t.Callable[[_T], bool]
) -> t.Optional[_T]:
    for item in sequence:
        if condition(item):
            return item
    return None


_BR_RE = re.compile(r"\s*<br/?>\s*")


def md(html_or_markdown_or_something: str, html: bool = False) -> Markdown:
    html_or_markdown_or_something = html_or_markdown_or_something.strip()
    if html:
        html_or_markdown_or_something = markdownify(html_or_markdown_or_something)
    else:
        html_or_markdown_or_something = _BR_RE.sub("\n", html_or_markdown_or_something)
    return Markdown(html_or_markdown_or_something)
