import typing as t
from dataclasses import dataclass
from datetime import timedelta
from urllib.parse import urljoin

from bs4 import BeautifulSoup
from bs4.element import Tag
from rich.prompt import Prompt

from .console import console
from .menu import Menu
from .mpv import mpv
from .requests import session
from .util import FAILDUMP_FILE, format_duration

RELIVE_URL = "https://streaming.media.ccc.de/rc3/relive"

_T_Relive = t.TypeVar("_T_Relive", bound="Relive")


@dataclass
class Relive:
    title: str
    room: str
    duration: timedelta
    _href: str
    _details: t.Optional[BeautifulSoup] = None

    @property
    def menu_line(self) -> str:
        minutes = format_duration(self.duration)
        return f"{self.title} [yellow]{self.room}[/yellow] [cyan]{minutes}[/cyan]"

    @property
    def menu_description(self) -> t.Optional[str]:
        return None

    @property
    def media_url(self) -> t.Optional[str]:
        if self._details is None:
            with console.status(
                f"Downloading relive details for [i]{self.title}[/i] ..."
            ):
                resp = session.get(self._href)
                resp.raise_for_status()
                self._details = BeautifulSoup(resp.text, "html.parser")
        pl = self._details.find("div", {"class": "video-wrap"})
        if not isinstance(pl, Tag):
            raise TypeError(f".video-wrap not a tag: {pl!r}")
        ds = pl["data-source"]
        if not isinstance(ds, str):
            raise RuntimeError(f"data-source not a string: {ds!r}")
        if pl:
            return urljoin("https://streaming.media.ccc.de/", ds)
        else:
            with open(FAILDUMP_FILE, "w") as ff:
                print(self._details, file=ff)
            return None

    def play(self) -> None:
        url = self.media_url
        if url:
            mpv(url, self.title)
        else:
            Prompt.ask(
                f"[b]{self.title}[/b]\n[red]ERROR:[/red]"
                f" No media URL, see [u]{FAILDUMP_FILE}[/u]\n"
                "Press enter to continue"
            )

    @classmethod
    def fetch(cls: t.Type[_T_Relive]) -> t.Iterator[_T_Relive]:
        resp = session.get(RELIVE_URL)
        resp.raise_for_status()
        soup = BeautifulSoup(resp.text, "html.parser")
        for rec in soup.find_all("div", "recording"):
            title = rec.find("h3", {"class": "panel-title"}).a
            metadata = rec.find("ul", {"class": "metadata"})

            duration_s = "".join(
                metadata.find("span", {"class": "fa-clock-o"}).parent.stripped_strings
            ).strip()
            if duration_s.endswith(" min"):
                duration = timedelta(minutes=int(duration_s[:-4]))
            else:
                console.log(f"Weird duration: {duration_s!r}")
                duration = timedelta()
            yield cls(
                title=title.string.strip(),
                duration=duration,
                room="".join(
                    metadata.find(
                        "span", {"class": "fa-sign-in"}
                    ).parent.stripped_strings
                ),
                _href=urljoin("https://streaming.media.ccc.de/", title["href"]),
            )


class ReliveMenu(Menu):
    menu_line = "Relive"
    prompt = "Select relive"

    def items(self) -> tuple[Relive, ...]:
        with console.status("Downloading relive index"):
            return tuple(Relive.fetch())
