import json
import typing as t
from dataclasses import dataclass
from datetime import datetime, timedelta

import dateutil.parser
from bs4 import BeautifulSoup

from .console import console
from .requests import session
from .util import TZ, first, parse_duration

FAHRPLAN_URL = "https://rc3.world/2021/public_fahrplan"
# https://fahrplan.events.ccc.de/rc3/2021/Fahrplan/


T_Entry = t.TypeVar("T_Entry", bound="Entry")


@dataclass
class Entry:
    description_html: str
    language: str
    room: str
    duration: timedelta
    start: datetime
    end: datetime
    speakers: str  # TODO: split?
    title: str
    track_name: str

    @classmethod
    def from_data(cls: t.Type[T_Entry], data: t.Any) -> T_Entry:  # FIXME: Any
        duration = parse_duration(data["schedule_duration"])
        start = dateutil.parser.parse(
            data["schedule_start"]
            .replace("noon", "12 p.m.")
            .replace("midnight", "12 a.m.")
            .replace("Dezember", "December"),
            ignoretz=True,
        ).replace(tzinfo=TZ)
        end = start + duration
        return cls(
            description_html=data["description_html"].strip(),
            language=data["language"],
            room=data["room_name"],
            duration=duration,
            start=start,
            end=end,
            speakers=data["speakers"],
            title=data["title"].strip(),
            track_name=data["track_name"],
        )

    @classmethod
    def fetch(cls: t.Type[T_Entry]) -> list[T_Entry]:
        resp = session.get(FAHRPLAN_URL)
        resp.raise_for_status()
        soup = BeautifulSoup(resp.text, "html.parser")
        return sorted(
            (
                cls.from_data(
                    json.loads(
                        entry.find("script", {"type": "application/json"}).string
                    )
                )
                for entry in soup.find_all("a", "rc3-fahrplan__link")
            ),
            key=lambda e: e.start,
        )


class Fahrplan:
    STALE = timedelta(hours=1)
    freshness: datetime
    entries: list[Entry]

    def __init__(self) -> None:
        self.freshness = datetime.min
        self.refresh()

    def refresh(self, force: bool = False) -> None:
        if force or datetime.now() - self.freshness > self.STALE:
            with console.status("Refreshing Fahrplan"):
                self.entries = Entry.fetch()
            self.freshness = datetime.now()

    def at(
        self, room: str, now: t.Optional[datetime] = None
    ) -> tuple[t.Optional[Entry], t.Optional[Entry]]:
        self.refresh()
        here = [e for e in self.entries if e.room == room]
        now2 = now or datetime.now(tz=TZ)
        return (
            first(here, lambda e: e.start <= now2 <= e.end),
            first(here, lambda e: e.start > now2),
        )
