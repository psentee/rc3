import typing as t
import xml.etree.ElementTree as ET
from dataclasses import dataclass
from datetime import datetime, timedelta

from rich.console import RenderableType

from .console import console
from .menu import Menu
from .mpv import mpv
from .requests import session
from .util import format_duration, md, parse_duration

RECORDING_URL = "https://media.ccc.de/c/rc3-2021/podcast/webm-hq.xml"  # correct?


_T_Recording = t.TypeVar("_T_Recording", bound="Recording")


def _must_find(elt: ET.Element, path: str) -> ET.Element:
    found = elt.find(path)
    if found is None:
        raise ValueError(f"Could not find {path!r} in {elt}")
    return found


def _must_text(elt: ET.Element, path: str) -> str:
    return _must_find(elt, path).text or ""


@dataclass
class Recording:
    title: str
    link: str
    description: str
    media_url: str
    media_size: int
    date: datetime
    identifier: str
    author: str
    keywords: set[str]
    duration: timedelta

    def __str__(self) -> str:
        kwkw = ", ".join(sorted(self.keywords))
        return f"{self.date}\t{self.title} ({self.author}) {self.duration}\t{kwkw}"

    @property
    def menu_line(self) -> str:
        kwkw = ", ".join(sorted(f"[u]{kw}[/u]" for kw in self.keywords))
        minutes = format_duration(self.duration)
        return f"{self.title}  [i]{self.author}[/i]  [cyan]{minutes}[/cyan]  [yellow]{kwkw}[/yellow]"  # noqa: E501

    @property
    def menu_description(self) -> t.Optional[RenderableType]:
        return md(self.description)

    def play(self) -> None:
        mpv(self.media_url, self.title, md(self.description))

    @classmethod
    def from_xml(cls: t.Type[_T_Recording], elt: ET.Element) -> _T_Recording:
        title = _must_text(elt, "./title")
        if title.endswith(" (rc3)"):
            title = title[:-6]
        enclosure = _must_find(elt, "./enclosure").attrib
        return cls(
            title=title,
            link=_must_text(elt, "./link"),
            description=_must_text(elt, "./description"),
            media_url=enclosure["url"],
            media_size=int(enclosure["length"]),
            date=datetime.fromisoformat(
                _must_text(elt, "./{http://purl.org/dc/elements/1.1/}date")
            ),
            identifier=_must_text(
                elt, "./{http://purl.org/dc/elements/1.1/}identifier"
            ),
            author=_must_text(
                elt, "./{http://www.itunes.com/dtds/podcast-1.0.dtd}author"
            ),
            keywords={
                kw.strip()
                for kw in _must_text(
                    elt, "./{http://www.itunes.com/dtds/podcast-1.0.dtd}keywords"
                ).split(",")
            },
            duration=parse_duration(
                _must_text(
                    elt, "./{http://www.itunes.com/dtds/podcast-1.0.dtd}duration"
                )
            ),
        )

    @classmethod
    def fetch(cls: t.Type[_T_Recording]) -> tuple[str, list[_T_Recording]]:
        resp = session.get(RECORDING_URL)
        resp.raise_for_status()
        root = ET.fromstring(resp.text)

        return (
            _must_text(root, "./channel/lastBuildDate"),
            sorted(
                ((cls.from_xml(item) for item in root.findall("./channel/item"))),
                key=lambda r: r.date,
                reverse=True,
            ),
        )


class RecordingMenu(Menu):
    menu_line = "Recordings"
    prompt = "Select recording"

    def items(self) -> tuple[Recording, ...]:
        with console.status("Downloading recordings index"):
            updated, recordings = Recording.fetch()
            self.header = f"Last update: {updated}"
            return tuple(recordings)
