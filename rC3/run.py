import subprocess
import typing as t

from rich.prompt import Prompt

from .console import console


def run(command: str, *args: str, cwd: t.Optional[str] = None) -> bool:
    console.print(f"[yellow]+[/yellow] [bold]{command}[/bold] {' '.join(args)}")
    cp = subprocess.run((command, *args), cwd=cwd, check=False)
    if cp.returncode != 0:
        Prompt.ask(
            f"[red]ERROR?[/red] [b]{command}[/b] seems to have failed ([u]{cp}[/u]).\n"
            "Hit enter after reading whatever error message it printed"
        )
        return False
    return True
